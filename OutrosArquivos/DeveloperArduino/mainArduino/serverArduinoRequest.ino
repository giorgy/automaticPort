
void serverArduinoRequest(EthernetClient cliente){
  /*
    função recebe a solicitação para abrir a porta via aplicativo web, encaminhado diretamente pelo servidor Web.
    Sua função é abrir ler o request e chamar a função que cuida da manipulação da porta.
  */
  
  String readString;

    //Verifica se existe realmente um cliente com um request para ser analizado
    while (cliente.connected()) {
      if (cliente.available()) {
        char c = cliente.read();

    
        if (readString.length() < 200) {
          readString += c;

            // Finaliza a conexão com servidor, após realizar  a leitura da primeira linha da requisição do servidor
            if (c == '\n'){
            cliente.println("HTTP/1.1 200 OK");
            cliente.println("Content-Type: text/html");
            cliente.println();
            cliente.stop();
            readString="";
            
            //Chama a abertura da porta, independente do resultado do servidor
            managerArduinoDoor();
            break;
          }
        }
      }
    }
 }
