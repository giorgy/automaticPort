void setup(){
    // Start the hardware serial port
    Serial.begin(9600);
    
    SPI.begin();        // Inicia  SPI bus
    mfrc522.PCD_Init(); // inicia  mfrc522
    
    //Leitores de Cartão RFID
    //portOne.begin(9600);

    //Iniciar conexão de rede com Arduíno
    Ethernet.begin(mac,ip,gateway,subnet);

    pinMode(pinDoor, OUTPUT); // Configura o pino do led (digital) como saída
 
}
