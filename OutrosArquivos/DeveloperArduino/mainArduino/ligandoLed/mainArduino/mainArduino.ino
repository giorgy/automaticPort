#include <SoftwareSerial.h>
#include <Ethernet.h>
#include <SPI.h>
#include <MFRC522.h>
#include <HttpClient.h>
#define SS_PIN 53  
#define RST_PIN 5


MFRC522 mfrc522(SS_PIN, RST_PIN); 





// Configuração da cominucação entre os Leitores RFID
//SoftwareSerial portOne(2,3);
EthernetClient clientArduino;
EthernetServer serverArduino(80);



//Configuração da interface de rede do Arduíno
IPAddress ip(191, 52, 62, 36); 
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
byte gateway[] = { 191, 52, 62, 1 }; 
byte subnet[] = { 255, 255, 255, 192 };

//Configuração do Cliente/Servidor Arduíno 
//EthernetServer serverArduino(80); //conexao da porta do arduino

//Configuração do Servidor de Aplicação
byte ipServerApplication[] = {191, 52, 62, 21};
int pinDoor = 13; 

void readCard(void);
void sendRequestToServer(String);
void serverArduinoRequest(EthernetClient);
void managerArduinoDoor();
void fechar_Porta();
void abrir_Porta();
