void readCard(){
  // Mostra UID na serial
  Serial.print("UID da tag :");
  String conteudo= "";
  byte letra;
  
  for (byte i = 0; i < mfrc522.uid.size; i++) 
  {
     conteudo.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? "0" : ""));
     conteudo.concat(String(mfrc522.uid.uidByte[i], HEX));

    // concat Acrescenta o parâmetro para uma String
    // uid seria a informacao do cartao    
  }
  
  conteudo.toUpperCase(); // maisculo 
  sendRequestToServer(String(conteudo)); 
}
