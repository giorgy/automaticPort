#include <Ethernet.h>
#include <SPI.h>
#include <MFRC522.h>
#define SS_1_PIN 53   
#define RST_PIN 5.

//#include "DHT.h"
// 
//#define DHTPIN A9 // pino que estamos conectado
//#define DHTTYPE DHT11 // DHT 
//DHT dht(DHTPIN, DHTTYPE);

/*
* Signal     Pin              Pin               Pin
*            Arduino Uno      Arduino Mega      MFRC522 board
* ------------------------------------------------------------
* Reset      9                5                 RST
* SPI SS     10               53                SDA
* SPI MOSI   11               51                MOSI
* SPI MISO   12               50                MISO
* SPI SCK    13               52                SCK
//Configuração das portas de Controle do Leitor de Cartão
*/

MFRC522 mfrc522(SS_1_PIN, RST_PIN); 

//Configuração da interface de rede do Arduíno

byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
IPAddress ip(191, 52, 62, 19);
IPAddress server(191, 52, 62, 18);

//Configuração do Cliente/Servidor Arduíno 
EthernetServer serverArduino(8080);


//Configuração dos Pinos de Led Red = Porta fechada, Green = Porta Aberta
int pinDoorRed = 41;
int pinDoorYellow = 42;
int pinDoorGreen = 43;
int pinOpenCloseDoor = 40;

