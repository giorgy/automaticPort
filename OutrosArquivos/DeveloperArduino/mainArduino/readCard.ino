
void readCard(){
  /*
   A cada interupção doleitor, está função é chamada para realizar a leitura da tag RFID presente no leitor e,
   após finalizar a leitura, chama a função sendRequestToServer, que encaminha a a tag RFID para o server web analizar,
   se a porta pode ser aberta, ou não.
  */
  
  
  // Mostra UID na serial
  String conteudo= "";
  byte i;
  
  for (byte i = 0; i < mfrc522.uid.size; i++) 
  {
     conteudo.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? "0" : ""));
     conteudo.concat(String(mfrc522.uid.uidByte[i], HEX));

    // concat Acrescenta o parâmetro para uma String
    // uid seria a informacao do cartao    
  }
  conteudo.toUpperCase();
  Serial.println(conteudo); 
  managerArduinoDoor();
//  sendRequestToServer(String(conteudo)); 
}
