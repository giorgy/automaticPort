//Escopo das funções
void managerArduinoDoor();
void fechar_Porta();
void abrir_Porta();
void aguarde();

void managerArduinoDoor(){
  abrir_Porta();
  delay(1000);
  fechar_Porta();

}


void fechar_Porta(){
  Serial.println("Porta Fechada");
  digitalWrite(pinDoorGreen, LOW);
  digitalWrite(pinDoorRed, HIGH);
  digitalWrite(pinOpenCloseDoor, HIGH);


}

void abrir_Porta(){
  Serial.println("Porta Aberta");
  
  //controle de LEDs
  digitalWrite(pinDoorRed, LOW);
  digitalWrite(pinDoorYellow, HIGH);
  delay(1500);
  
  //Controle da fechadura (abre e fecha)
  digitalWrite(pinOpenCloseDoor, LOW);
  delay(60);
  digitalWrite(pinOpenCloseDoor, HIGH);
  
  //controle de LEDs
  digitalWrite(pinDoorYellow, LOW);
  digitalWrite(pinDoorGreen, HIGH);
  digitalWrite(pinDoorRed, LOW);
 


}

void aguarde(){
  Serial.println("Aguarde");
  digitalWrite(pinDoorRed, LOW);
  digitalWrite(pinDoorYellow, HIGH);
  delay(1500);
  digitalWrite(pinDoorYellow, LOW);
  digitalWrite(pinDoorRed, HIGH);
}
