#coding=utf-8

from mongoengine import *
from django.contrib.auth import login

MONGODB_USER = 'sysca_user'
MONGODB_PASSWD = 'sysca_password'
MONGODB_HOST = 'localhost'
MONGODB_NAME = 'sysca_db'
MONGODB_DATABASE_HOST = 'mongodb://%s:%s@%s/%s' \
    % (MONGODB_USER, MONGODB_PASSWD, MONGODB_HOST, MONGODB_NAME)

connect(MONGODB_NAME, host=MONGODB_DATABASE_HOST)

class Perfil(Document):
    nome = StringField(max_length=120, required=True)

class Usuario(Document):
    nome = StringField(max_length=120, required=True)
    idade = IntField(max_length=5, required=True)
    email = EmailField(required=True)
    sexo = StringField(max_length=10)
    perfil = ReferenceField(Perfil)

class Bloco(Document):
    nome = StringField(max_length=120, required=True)
    tipo = StringField(max_length=120, required=True)

class PinList(EmbeddedDocument):
    pinoPorta = IntField(max_length=5)
    pinoLeitorRFID = IntField(max_length=5)

class Microcontrolador(Document):
    nome = StringField(max_length=120, required=True)
    tipo = IntField(max_length=5, required=True)
    macAddress = ListField(IntField())
    address = ListField(IntField())
    netmask = ListField(IntField())
    pinList = EmbeddedDocumentField(PinList)

class UsuariosAutorizados(EmbeddedDocument):
    usuario = ReferenceField(Usuario)
    horario = ListField(StringField(max_length=30))
    diasSemana = ListField(StringField(max_length=30))

class Ambiente(Document):
    nome = StringField(max_length=120, required=True)
    capacidade = IntField(max_length=5, required=True)
    bloco = ReferenceField(Bloco)
    usuariosAutorizados = ListField(EmbeddedDocumentField(UsuariosAutorizados))
    microcontrolador = ReferenceField(Microcontrolador)


class Usuario2(Document):
    user = StringField(max_length=30)
    password = StringField(max_length=50)

'''
perfil = perfil(nome="Administrador")
perfil.save()

usuario = Usuario(nome="giorgy", idade=29,email="giorgyismael@gmail.com",sexo="masculino", perfil=perfil)
usuario.save()

usuario2 = Usuario(nome="Veridiana", idade=28,email="veridiana@gmail.com",sexo="feminino", perfil=perfil)
usuario2.save()

bloco = Bloco(nome="Bloco B", tipo="Administrativo")
bloco.save()

microcontrolador = Microcontrolador(nome="ESP8266", tipo=12, macAddress=[192,168,0,2], address=[255,255,255,0], netmask=[192,168,0,1])
pinlist = PinList(pinoPorta=10, pinoLeitorRFID=12)
microcontrolador.pinList = pinlist
microcontrolador.save()

ambiente = Ambiente(nome="A-2", capacidade=40, bloco=bloco)
usuarioAutorizado1 = UsuariosAutorizados(usuario=usuario, horario=["12:00","18:00"], diasSemana=["Segunda","Terça"])
usuarioAutorizado2 = UsuariosAutorizados(usuario=usuario2, horario=["08:00","18:00"], diasSemana=["Terça","Quinta"])

ambiente.usuariosAutorizados.append(usuarioAutorizado1)
ambiente.usuariosAutorizados.append(usuarioAutorizado2)
ambiente.microcontrolador = microcontrolador
ambiente.save()

usuario=Usuario2(user="giorgyismael@gmail", password="123456")
usuario.save()
'''
request = {"user":"giorgyismael@gmail", "password":"123456"}
user = Usuario2.objects(user__iexact="giorgyismael@gmail")[0]
print(user)
if (user):
    if(request["password"]==user.password):
        print("usuario logado")


